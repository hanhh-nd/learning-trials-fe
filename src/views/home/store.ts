import learningTrialApiService from '@/common/service/learning-trial.api.service';
import { defineStore } from 'pinia';
import { ref } from 'vue';
import { IMessage } from './interfaces';

export const useHomeStore = defineStore('home', () => {
    const messageList = ref<IMessage[]>([]);

    async function getMessageList() {
        const response = await learningTrialApiService.start();
        if (response?.success) {
            const responseData = response?.data.messages || [];
            const data = responseData.map(
                (data) => ((data.is_shown = true), data),
            );
            data[data.length - 1].is_shown = false;
            SET_MESSAGE_LIST(data);
        } else {
            SET_MESSAGE_LIST([]);
        }
    }

    function SET_MESSAGE_LIST(_messageList: IMessage[]) {
        messageList.value = _messageList;
    }

    async function getMessage(id: string | number) {
        const response = await learningTrialApiService.getMessage(id);
        if (response?.success) {
            APPEND_MESSAGE(response?.data);
        }
    }

    function appendMessage(message: IMessage) {
        APPEND_MESSAGE(message);
    }

    function APPEND_MESSAGE(message: IMessage) {
        messageList.value.push(message);
    }

    return {
        messageList,
        getMessageList,
        getMessage,
        appendMessage,
    };
});

import { MessageType } from './constants';

export type IMessageTextContent = string;
export type IMessageButtonContent = {
    title: string;
    value: string;
    type: string;
    navigate_to_id: string | null;
};
export type IMessageMediaContent = string;
export interface IMessageChoice {
    title: string;
    value: string;
    is_correct?: boolean;
    explanation?: string;
}
export type IMessageChoicesContent = {
    question: string;
    choices: IMessageChoice[] | IMessageButtonContent[];
    user_answer?: IMessageAnswer;
};
export type IMessageReviseContent = {
    revise_id: number;
    listQuestion?: IQuestion[];
};
export type IQuestion = {
    question_id: number;
    folder_id: number;
    content: string;
    stripped_content: string;
    type_id: number;
    answers: IAnswer[];
    user_answer?: IMessageAnswer;
};
export interface IAnswer {
    content: string;
    is_true: boolean;
}
export interface IMessageAnswer {
    answer_content: string;
}
export interface IMessage {
    _id: string;
    script_id: number;
    parent_id: string | null;
    navigate_to_id: string | null;
    type: MessageType;
    content:
        | IMessageTextContent
        | IMessageMediaContent
        | IMessageChoicesContent
        | IMessageReviseContent;
    is_user: boolean;
    is_shown?: boolean;
    ancestor_ids: string[];
}

export interface IUpdateMessageAnswerBody {
    messageId: string;
    answer: string;
}

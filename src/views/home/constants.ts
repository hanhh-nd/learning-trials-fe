export enum MessageType {
    GROUP = 'GROUP',
    TEXT = 'TEXT',
    RICH_TEXT = 'RICH_TEXT',
    BUTTONS = 'BUTTONS',
    MEDIA = 'MEDIA',
    SINGLE_CHOICE = 'SINGLE_CHOICE',
    PRACTICE = 'PRACTICE',
}

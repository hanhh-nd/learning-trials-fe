import './assets/main.css';

import ElementPlus from 'element-plus';
import { createApp } from 'vue';
import App from './App.vue';
import pinia from './plugins/pinia';
import router from './router';
import './assets/styles/bootstrap/bootstrap.min.css';
import './assets/styles/global.scss';

const app = createApp(App);

app.use(router).use(ElementPlus).use(pinia);

app.mount('#app');

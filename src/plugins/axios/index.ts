import axios, { AxiosRequestConfig } from 'axios';
import CommonMiddleware from './middlewares/commonMiddleware';
import HttpMiddlewareService from './service';

const options: AxiosRequestConfig = {
    baseURL: import.meta.env.VITE_APP_API_URL,
    responseType: 'json',
    headers: {
        Authorization:
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ODg3NiwiZGV2aWNlSWQiOjg4NzYsImlhdCI6MTY5MzI3Njg5MywiZXhwIjoxNjk4NDYwODkzfQ.LndwX0ref-Yrrfkn9FYKpABe36rvbXtlirZsaWJYc3Y',
    },
};

const axiosInstance = axios.create(options);
const axiosService = new HttpMiddlewareService(axiosInstance);
axiosService.register([new CommonMiddleware()]);

export default axiosInstance;

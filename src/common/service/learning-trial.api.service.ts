import { IBodyResponse } from '@/common/interfaces';
import { ApiService } from '@/common/service/api';
import axiosService from '@/plugins/axios';
import { IMessage, IUpdateMessageAnswerBody } from '@/views/home/interfaces';

class LearningTrialApiService extends ApiService {
    start(): Promise<IBodyResponse<{ messages: IMessage[] }>> {
        return this.client.get(`${this.baseUrl}/chats/start`);
    }

    getMessage(id: number | string): Promise<IBodyResponse<IMessage>> {
        return this.client.get(`${this.baseUrl}/messages/${id}`);
    }

    updateMessageAnswer(
        body: IUpdateMessageAnswerBody,
    ): Promise<IBodyResponse<boolean>> {
        return this.client.patch(
            `${this.baseUrl}/chats/update-message-answer`,
            body,
        );
    }
}
const learningTrialApiService = new LearningTrialApiService(
    { baseUrl: `/learning-trials` },
    axiosService,
);

export default learningTrialApiService;

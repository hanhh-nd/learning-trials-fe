import {
    Observable,
    concat,
    delay,
    ignoreElements,
    interval,
    map,
    of,
    take,
} from 'rxjs';
import { Ref, onBeforeUnmount, ref } from 'vue';

function subscribeTo<T>(
    observable: Observable<T>,
    next?: (value: T) => void,
    error?: (err: any) => void,
    complete?: () => void,
) {
    const subscription = observable.subscribe(next, error, complete);
    onBeforeUnmount(() => {
        subscription.unsubscribe();
    });

    return subscription;
}

export function useObservable<T>(
    observable: Observable<T>,
    complete?: () => void,
): Ref<T> {
    const handler = ref() as Ref<T>;
    subscribeTo(
        observable,
        (value) => {
            handler.value = value;
        },
        (error) => {
            throw error;
        },
        complete,
    );
    return handler;
}

export const type = ({ word, speed }: { word: string; speed: number }) => {
    return interval(speed).pipe(
        map((x) => {
            const w = word.slice(0, x + 1);
            if (['<', '/', '>'].includes(w[w.length - 1])) {
                const lastIndex = w.lastIndexOf('<');
                return word.slice(0, lastIndex);
            }
            return w;
        }),
        take(word.length),
    );
};

export const typeEffect = (word: string, play: boolean = true) => {
    if (!play) return [word];

    return concat(
        type({ word, speed: 50 }), // type forwards
        of('').pipe(delay(1200), ignoreElements()), // pause
    );
};
